// React imports
import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

const Home = lazy(() => import('./pages/Home'));

const AppRouter = () => {
    return (
        <Router>
            <Routes>
                <Route path='/' element={
                    <Suspense fallback={<div>Loading...</div>}>
                        <Home />
                    </Suspense>
                } />
            </Routes>
        </Router>
    )
}

export default AppRouter;